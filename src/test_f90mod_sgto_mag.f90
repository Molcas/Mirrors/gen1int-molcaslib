!!  gen1int: compute one-electron integrals using rotational London atomic-orbital
!!  Copyright 2009-2012 Bin Gao, and Andreas Thorvaldsen
!!
!!  gen1int is free software: you can redistribute it and/or modify
!!  it under the terms of the GNU Lesser General Public License as published by
!!  the Free Software Foundation, either version 3 of the License, or
!!  (at your option) any later version.
!!
!!  gen1int is distributed in the hope that it will be useful,
!!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
!!  GNU Lesser General Public License for more details.
!!
!!  You should have received a copy of the GNU Lesser General Public License
!!  along with gen1int. If not, see <http://www.gnu.org/licenses/>.
!!
!!  2012-03-20, Bin Gao
!!  * first version


    subroutine test_f90mod_sgto_mag(iblock, jblock,                &
                                    num_sgto_bra,num_sgto_ket,     &
                                    num_contr_bra,num_contr_ket,   &
                                    angular_bra,angular_ket,       &
                                    num_prim_bra,num_prim_ket,     &
                                    idx_bra,idx_ket,               &
                                    exponent_bra,exponent_ket,     &
                                    coef_bra,coef_ket,num_atoms,   &
                                    coords,ncomp,contr_int,        &
                                    logxtc, cur_atom, dotran)
    use xkind
    use gen1int
    use html_log
    implicit none
    integer iblock, jblock, ncomp       !incremental recorders over blocks of sub-shells
    integer idx_bra, idx_ket            !indices of bra and ket centers
    integer num_sgto_bra, num_sgto_ket  !number of spherical Gaussians of bra and ket centers
    integer num_atoms, cur_atom         !number of atoms and i'th atom in molcas (see comment below)
    type(one_prop_t) one_prop           !one-electron property integral operator
    real(REALK), allocatable :: contr_ints(:,:,:,:,:)
    integer idx_nuclei(num_atoms)
    real(REALK) contr_int(num_sgto_bra * num_contr_bra * &
                          num_sgto_ket * num_contr_ket * ncomp)
    real(REALK) coords(3*num_atoms)
    real(REALK), allocatable :: coords_nuc(:,:)
    integer num_prop !number of property integral matrices
    integer num_contr_bra, angular_bra, num_prim_bra
    integer num_contr_ket, angular_ket, num_prim_ket
    real(REALK) exponent_bra(num_prim_bra), exponent_ket(num_prim_ket)
    real(REALK) coef_bra(num_contr_bra*num_prim_bra)
    real(REALK) nrm_bra(num_contr_bra*num_prim_bra)
    real(REALK) coef_ket(num_contr_ket*num_prim_ket)
    real(REALK) nrm_ket(num_contr_ket*num_prim_ket)
    real(REALK) nrm_contr_coef_bra(num_contr_bra*num_prim_bra)
    real(REALK) nrm_contr_coef_ket(num_contr_ket*num_prim_ket)

    integer ierr                        !error information
    integer nprop, iprop, idx_bra_start, idx_bra_end
    integer mag_ord_ket(num_sgto_ket), mag_ord_bra(num_sgto_bra)
    integer iopt,i,j,ij,ii,jj,iii,N,at,iik,iib
    !real, parameter :: ALPHA2=0.00005325135452782264
    logical logxtc, dotran


    nprop = ncomp * num_atoms

    do i = 1, num_atoms
      idx_nuclei(i) = i
    enddo

    allocate(coords_nuc(3, num_atoms), stat=ierr)
    i = 0
    j = 0
    ij = 0
    do i = 1, num_atoms
      do j = 1, 3
        ij = ij + 1
        coords_nuc(j,i) = coords(ij)
      enddo
    enddo

    call OnePropCreate(prop_name=INT_PSO,    &
                       one_prop=one_prop,    &
                       info_prop=ierr,       &
                       idx_nuclei=idx_nuclei,&
                       coord_nuclei=coords_nuc)

    call OnePropGetNumProp(one_prop=one_prop,&
                           num_prop=num_prop)


    i = 0
    j = 0
    ij = 0
    do i = 1, num_prim_bra
      do j = 0, num_contr_bra - 1
        ij = ij + 1
        nrm_bra(ij) = coef_bra(i + j * num_prim_bra)
      enddo
    enddo

    i = 0
    j = 0
    ij = 0
    do i = 1, num_prim_ket
      do j = 0, num_contr_ket - 1
        ij = ij + 1
        nrm_ket(ij) = coef_ket(i + j * num_prim_ket)
      enddo
    enddo


    call norm_contr_sgto(angular_bra,  num_prim_bra, &
                         exponent_bra, num_contr_bra, nrm_bra)
    call norm_contr_sgto(angular_ket,  num_prim_ket, &
                         exponent_ket, num_contr_ket, nrm_ket)

    nrm_contr_coef_bra = nrm_bra
    nrm_contr_coef_ket = nrm_ket


    if (.not. dotran) then
      ! allocates the contracted integrals 
      allocate(contr_ints(num_sgto_bra, num_contr_bra, &
                          num_sgto_ket, num_contr_ket, &
                          nprop), stat=ierr)
      call OnePropGetIntegral(idx_bra=idx_bra,                   &
                              coord_bra=coords_nuc(:,idx_bra),   &
                              angular_bra=angular_bra,           &
                              num_prim_bra=num_prim_bra,         &
                              exponent_bra=exponent_bra,         &
                              num_contr_bra=num_contr_bra,       &
                              contr_coef_bra=nrm_contr_coef_bra, &
                              idx_ket=idx_ket,                   &
                              coord_ket=coords_nuc(:,idx_ket),   &
                              angular_ket=angular_ket,           &
                              num_prim_ket=num_prim_ket,         &
                              exponent_ket=exponent_ket,         &
                              num_contr_ket=num_contr_ket,       &
                              contr_coef_ket=nrm_contr_coef_ket, &
                              spher_gto=.true.,                  &
                              one_prop=one_prop,                 &
                              num_gto_bra=num_sgto_bra,          &
                              num_gto_ket=num_sgto_ket,          &
                              num_opt=nprop,                     &
                              contr_ints=contr_ints,             &
                              logxtc=logxtc)
    else
      ! allocates the contracted integrals 
      allocate(contr_ints(num_sgto_ket, num_contr_ket, &
                          num_sgto_bra, num_contr_bra, &
                          nprop), stat=ierr)
      call OnePropGetIntegral(idx_bra=idx_ket,                   &
                              coord_bra=coords_nuc(:,idx_ket),   &
                              angular_bra=angular_ket,           &
                              num_prim_bra=num_prim_ket,         &
                              exponent_bra=exponent_ket,         &
                              num_contr_bra=num_contr_ket,       &
                              contr_coef_bra=nrm_contr_coef_ket, &
                              idx_ket=idx_bra,                   &
                              coord_ket=coords_nuc(:,idx_bra),   &
                              angular_ket=angular_bra,           &
                              num_prim_ket=num_prim_bra,         &
                              exponent_ket=exponent_bra,         &
                              num_contr_ket=num_contr_bra,       &
                              contr_coef_ket=nrm_contr_coef_bra, &
                              spher_gto=.true.,                  &
                              one_prop=one_prop,                 &
                              num_gto_bra=num_sgto_ket,          &
                              num_gto_ket=num_sgto_bra,          &
                              num_opt=nprop,                     &
                              contr_ints=contr_ints,             &
                              logxtc=logxtc)
    endif
    deallocate(coords_nuc)

    i = 0
    j = 0
    ii = 0
    jj = 0
    iii = 0
    idx_bra_start = ncomp * (cur_atom - 1) + 1
    idx_bra_end   = idx_bra_start + ncomp - 1


    do i = 1, num_sgto_bra
      mag_ord_bra(i) = i
    enddo

    do i = 1, num_sgto_ket
      mag_ord_ket(i) = i
    enddo

    ! Molcas integral handling is restricted to symmetric or 
    ! anti-symmetric matrices.  To get around this for a full
    ! square matrix with neither property, we call the routines 
    ! twice, one time swapping all bras and kets and then pack 
    ! each as separate symmetric (lower triangular) matrices.
    ! Then when block-wise integral processing is completed,
    ! stitch them back together as full square matrices.
    ! Hence the "dotran" logical.
    if (.not. dotran) then
      do iprop = idx_bra_start, idx_bra_end
        do i = 1, num_sgto_bra
          mag_ord_bra(i) = i
        enddo
        do i = 1, num_sgto_ket
          mag_ord_ket(i) = i
        enddo
        ! The Molcas p function reordering works for
        ! the 1st, 4th and 7th components but is incorrect
        ! for the rest.  This is probably something nefarious
        ! deeper in the code but we fix it here for now and 
        ! move on with life.
        if (num_sgto_ket.eq.3 .and. .not. mod(iprop + 2, 3).eq.0) &
   &        mag_ord_ket(1:3) = (/ 3, 1, 2 /)
        if (num_sgto_bra.eq.3 .and. .not. mod(iprop + 2, 3).eq.0) &
   &        mag_ord_bra(1:3) = (/ 3, 1, 2 /)
        do ii = 1, num_sgto_ket
          iik = mag_ord_ket(ii)
          do i = 1, num_sgto_bra
            iib = mag_ord_bra(i)
            do jj = 1, num_contr_ket
              do j = 1, num_contr_bra
                iii = iii + 1
                contr_int(iii) = contr_ints(iib, j, iik, jj, iprop)
              enddo
            enddo
          enddo
        enddo
      enddo
    else
      do iprop = idx_bra_start, idx_bra_end
        do i = 1, num_sgto_bra
          mag_ord_bra(i) = i
        enddo
        do i = 1, num_sgto_ket
          mag_ord_ket(i) = i
        enddo
        ! Similar story as above, subsequent p function
        ! reordering is necessary but only for some components.
        if (num_sgto_ket.eq.3 .and. .not. mod(iprop + 2, 3).eq.0) &
   &        mag_ord_ket(1:3) = (/ 3, 1, 2 /)
        if (num_sgto_bra.eq.3 .and. .not. mod(iprop + 2, 3).eq.0) &
   &        mag_ord_bra(1:3) = (/ 3, 1, 2 /)
        do ii = 1, num_sgto_ket
          iik = mag_ord_ket(ii)
          do i = 1, num_sgto_bra
            iib = mag_ord_bra(i)
            do jj = 1, num_contr_ket
              do j = 1, num_contr_bra
                iii = iii + 1
                contr_int(iii) = contr_ints(iik, jj, iib, j, iprop)
              enddo
            enddo
          enddo
        enddo
      enddo
    endif

    deallocate(contr_ints)
    return

  end subroutine test_f90mod_sgto_mag
