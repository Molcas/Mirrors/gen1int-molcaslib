!!  gen1int: compute one-electron integrals using rotational London atomic-orbitals
!!  Copyright 2009-2012 Bin Gao, and Andreas Thorvaldsen
!!
!!  gen1int is free software: you can redistribute it and/or modify
!!  it under the terms of the GNU Lesser General Public License as published by
!!  the Free Software Foundation, either version 3 of the License, or
!!  (at your option) any later version.
!!
!!  gen1int is distributed in the hope that it will be useful,
!!  but WITHOUT ANY WARRANTY; without even the implied warranty of
!!  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
!!  GNU Lesser General Public License for more details.
!!
!!  You should have received a copy of the GNU Lesser General Public License
!!  along with gen1int. If not, see <http://www.gnu.org/licenses/>.
!!
!!  Tests tests kinetic energy integrals from Fortran 90 modules using SGTOs.
!!
!!  2012-03-20, Bin Gao
!!  * first version

  !> \brief tests kinetic energy integrals from Fortran 90 modules using SGTOs
  !> \author Bin Gao
  !> \date 2012-03-20
  !> \param io_log is the IO unit of log file
  !> \return test_failed indicates if the test is failed
  subroutine test_f90mod_sgto_angmom(BRA_BLOCK,KET_BLOCK,              &
                                     num_sgto_bra,num_sgto_ket,        &
                                     num_contr_bra,num_contr_ket,      &
                                     angular_bra,angular_ket,          &
                                     num_prim_bra,num_prim_ket,        &
                                     idx_bra,idx_ket,                  &
                                     exponent_bra,exponent_ket,        &
                                     coef_bra,coef_ket,N_ATOMS,COORDS,contr_int)
    use xkind
    ! Fortran 90 module of Gen1Int library
    use gen1int
    ! module of HTML test log routines
    use html_log
    implicit none
! water molecule using cc-pV6Z basis sets
!#include "water_ccpv6z.h"
!#include "NPO2.h"
!#include "water_geometry.h"
! test parameters and referenced results
!#include "water_ccpv6z_sgto_kinene_angmom.h"
    !integer itst                        !incremental recorder over test cases
    integer iblock, jblock              !incremental recorders over blocks of sub-shells
    integer idx_bra, idx_ket, N_ATOMS            !indices of bra and ket centers
    integer num_sgto_bra, num_sgto_ket  !number of spherical Gaussians of bra and ket centers
    type(one_prop_t) one_prop           !one-electron property integral operator
    !integer num_prop                    !number of property integral matrices
    real(REALK), allocatable :: contr_ints(:,:,:,:,:)
    !real(REALK), allocatable :: contr_int(:)
    real(REALK) contr_int(num_contr_bra*num_contr_ket*num_sgto_bra &
                *num_sgto_ket*3)
    real(REALK) COORDS(3*N_ATOMS)
    integer num_prop
    integer mag_bra(3)
    integer mag_ket(3)
    integer num_contr_bra, angular_bra, num_prim_bra
    integer num_contr_ket, angular_ket, num_prim_ket
    real(REALK) exponent_bra(num_prim_bra), exponent_ket(num_prim_ket)
    real(REALK) coef_bra(num_contr_bra*num_prim_bra)
    real(REALK) nrm_bra(num_contr_bra*num_prim_bra)
    real(REALK) coef_ket(num_contr_ket*num_prim_ket)
    real(REALK) nrm_ket(num_contr_ket*num_prim_ket)
    real(REALK) nrm_contr_coef_bra(num_contr_bra*num_prim_bra)
    real(REALK) nrm_contr_coef_ket(num_contr_ket*num_prim_ket)
    integer ierr                        !error information
    real(REALK) begin_time              !begin of CPU time
    real(REALK) end_time                !end of CPU time
    integer iopt,i,j,ij,ii,jj,iii                        !incremental recorder over operators
    integer icontr, jcontr              !incremental recorders over contractions
    integer isgto, jsgto                !incremental recorders over GTOs
    integer, parameter :: IDX_DIPORG = -1 ! indices of dipole origin
    ! coordinates of dipole origin
    real(REALK), parameter :: DIP_ORIGIN(3) = (/0.0_REALK, 0.0_REALK, 0.0_REALK/)
    
    integer BRA_BLOCK ! ID of block of sub-shells on bra center
    integer KET_BLOCK ! ID of block of sub-shells on ket center
     !  write(6,*)"COORDS",COORDS
    ! initializes the information of AngMom integrals
    call OnePropCreate(prop_name=INT_ANGMOM, one_prop=one_prop, &
                       info_prop=ierr, idx_diporg=idx_diporg,   &
                       dipole_origin=dip_origin)
      if (ierr/=0) stop "failed to call OnePropCreate!"
    ! gets the number of property integral matrices
    call OnePropGetNumProp(one_prop=one_prop, num_prop=num_prop)
      ! gets the blocks of sub-shells
      iblock = BRA_BLOCK!(itst)
      !write(6,*)"**BRA_BLOCK**",iblock
      jblock = KET_BLOCK!(itst)
      !write(6,*)"**KET_BLOCK**",jblock
      ! gets the begin time
      call xtimer_set(begin_time)
      ! gets the indices of bra and ket centers
      !write(6,*)"idx_bra",idx_bra
      !write(6,*)"idx_ket",idx_ket
      ! sets the number of spherical Gaussians of bra and ket centers
      !write(6,*)"**num_sgto_bra**",num_sgto_bra
      !write(6,*)"**num_sgto_ket**",num_sgto_ket
      !write(6,*)"**num_contr_bra**",num_contr_bra
      !write(6,*)"**num_contr_ket**",num_contr_ket
      !write(6,*)"**angular_bra**",angular_bra
      !write(6,*)"**angular_ket**",angular_ket
      !write(6,*)"**num_prim_bra**",num_prim_bra
      !write(6,*)"**num_prim_ket**",num_prim_ket
      ! allocates the contracted integrals
      allocate(contr_ints(num_sgto_bra,num_contr_bra, &
                          num_sgto_ket,num_contr_ket, &
                          num_prop), stat=ierr)

            ij=0
         do i=1,num_prim_bra
         do j=1,num_contr_bra 
           ij=ij+1
         nrm_bra(ij)= coef_bra(i+(j-1)*num_prim_bra)
          enddo
          enddo

      call norm_contr_sgto(angular_bra, num_prim_bra, exponent_bra, num_contr_bra,nrm_bra)

                    nrm_contr_coef_bra=0
                 nrm_contr_coef_bra=nrm_bra

              i=0
              j=0
              ij=0
         do i=1,num_prim_ket
         do j=1,num_contr_ket
           ij=ij+1
         nrm_ket(ij)= coef_ket(i+(j-1)*num_prim_ket)
          enddo
          enddo

       call norm_contr_sgto(angular_ket, num_prim_ket, exponent_ket, num_contr_ket, nrm_ket)

                      nrm_contr_coef_ket=0
                  nrm_contr_coef_ket= nrm_ket

         ! computes the contracted integrals
        !   if (angular_bra.ne.1.and.angular_ket.ne.1) then
        !       write(6,*) "herePSO_ angular_bra.ne.1.and.angular_ket.ne.1"
      call OnePropGetIntegral(idx_bra=idx_bra, coord_bra=COORDS(3*idx_bra-2:3*idx_bra),  &
                              angular_bra=angular_bra,                                   &
                              num_prim_bra=num_prim_bra,                                 &
                              exponent_bra=exponent_bra,                                 &
                              num_contr_bra=num_contr_bra,                               &
                              contr_coef_bra=nrm_contr_coef_bra,                         &
                              idx_ket=idx_ket, coord_ket=COORDS(3*idx_ket-2:3*idx_ket),  &
                              angular_ket=angular_ket,                                   &
                              num_prim_ket=num_prim_ket,                                 &
                              exponent_ket=exponent_ket,                                 &
                              num_contr_ket=num_contr_ket,                               & 
                              contr_coef_ket=nrm_contr_coef_ket,                         &
                              spher_gto=.true., one_prop=one_prop,                       &
                              num_gto_bra=num_sgto_bra, num_gto_ket=num_sgto_ket,        &
                              num_opt=num_prop, contr_ints=contr_ints)
         ! else if (angular_bra.eq.1.and.angular_ket.eq.1) then 
         !     write(6,*) "hereAng_ angular_bra.eq.1.and.angular_ket.eq.1"      
         !    mag_bra=(/1, -1, 0/)
         !    mag_ket=(/1, -1, 0/)  
     ! call OnePropGetIntegral(idx_bra=idx_bra, coord_bra=COORDS(3*idx_bra-2:3*idx_bra),  &    
     !                         angular_bra=angular_bra,                                   &
     !                         num_prim_bra=num_prim_bra,                                 &
     !                         exponent_bra=exponent_bra,                                 &
     !                         num_contr_bra=num_contr_bra,                               &
     !                         contr_coef_bra=nrm_contr_coef_bra,                         &
     !                         idx_ket=idx_ket, coord_ket=COORDS(3*idx_ket-2:3*idx_ket),  &
     !                         angular_ket=angular_ket,                                   &
     !                         num_prim_ket=num_prim_ket,                                 &
     !                         exponent_ket=exponent_ket,                                 &
     !                         num_contr_ket=num_contr_ket,                               &
     !                         contr_coef_ket=nrm_contr_coef_ket,                         &
     !                         spher_gto=.true., one_prop=one_prop,                       &
     !                         num_gto_bra=num_sgto_bra, num_gto_ket=num_sgto_ket,        &
     !                         num_opt=num_prop, contr_ints=contr_ints,                   & 
     !                         mag_num_bra=mag_bra,mag_num_ket=mag_ket)
     !      else if (angular_bra.eq.1.and.angular_ket.ne.1) then
     !        write(6,*) "hereAng_ angular_bra.eq.1.and.angular_ket.ne.1"
     !             mag_bra=(/1, -1, 0/)
    !  call OnePropGetIntegral(idx_bra=idx_bra, coord_bra=COORDS(3*idx_bra-2:3*idx_bra),  &
    !                          angular_bra=angular_bra,                                   &
    !                          num_prim_bra=num_prim_bra,                                 &
    !                          exponent_bra=exponent_bra,                                 &
    !                          num_contr_bra=num_contr_bra,                               &
    !                          contr_coef_bra=nrm_contr_coef_bra,                         &
    !                          idx_ket=idx_ket, coord_ket=COORDS(3*idx_ket-2:3*idx_ket),  &
    !                          angular_ket=angular_ket,                                   &
    !                          num_prim_ket=num_prim_ket,                                 &
    !                          exponent_ket=exponent_ket,                                 &
    !                          num_contr_ket=num_contr_ket,                               &
    !                          contr_coef_ket=nrm_contr_coef_ket,                         &
    !                          spher_gto=.true., one_prop=one_prop,                       &
    !                          num_gto_bra=num_sgto_bra, num_gto_ket=num_sgto_ket,        &
    !                          num_opt=num_prop, contr_ints=contr_ints,                   &
    !                          mag_num_bra=mag_bra)
    !      else if (angular_bra.ne.1.and.angular_ket.eq.1) then
    !         write(6,*) "hereAng_ angular_bra.ne.1.and.angular_ket.eq.1"
    !            mag_ket=(/1, -1, 0/)
    !  call OnePropGetIntegral(idx_bra=idx_bra, coord_bra=COORDS(3*idx_bra-2:3*idx_bra),  &
    !                          angular_bra=angular_bra,                                   &
    !                          num_prim_bra=num_prim_bra,                                 &
    !                          exponent_bra=exponent_bra,                                 &
    !                          num_contr_bra=num_contr_bra,                               &
    !                          contr_coef_bra=nrm_contr_coef_bra,                         &
    !                          idx_ket=idx_ket, coord_ket=COORDS(3*idx_ket-2:3*idx_ket),  &
    !                          angular_ket=angular_ket,                                   &
    !                          num_prim_ket=num_prim_ket,                                 &
    !                          exponent_ket=exponent_ket,                                 &
    !                          num_contr_ket=num_contr_ket,                               &
    !                          contr_coef_ket=nrm_contr_coef_ket,                         &
    !                          spher_gto=.true., one_prop=one_prop,                       &
    !                          num_gto_bra=num_sgto_bra, num_gto_ket=num_sgto_ket,        &
    !                          num_opt=num_prop, contr_ints=contr_ints,                   &
    !                          mag_num_ket=mag_ket)
     !    endif
      ! allocate(contr_int(num_contr_bra*num_contr_ket*num_sgto_bra*num_sgto_ket*num_prop),stat=ierr)

                 !write(6,*)"num_opt",num_prop
                 !write(6,*)"num_gto_ket",num_sgto_ket
                 !write(6,*)"num_gto_bra",num_sgto_bra
                 !write(6,*)"num_contr_ket",num_contr_ket
                 !write(6,*)"num_contr_bra",num_contr_bra

               !Call RecPrt('contr_ints From GEN1INT in Molcas Format',&
                !            ' ',contr_ints,&
                 !          num_contr_bra*num_contr_ket, &
                  !         num_sgto_bra*num_sgto_ket*num_prop)
                 iopt=0
                 ii=0
                 i=0
                 jj=0
                 j=0
                 iii=0
               do iopt=1,num_prop
                !write(6,*)"iopt"
               do ii=1,num_sgto_ket
                !write(6,*)"ii"
               do i=1,num_sgto_bra
                !write(6,*)"i"
               do jj=1,num_contr_ket
                 !write(6,*)"jj"
               do j=1,num_contr_bra
                 iii=iii+1
               contr_int(iii) = contr_ints(i,j,ii,jj,iopt)
                 enddo
                 enddo
                 enddo
                 enddo
                 enddo
               ! write(6,*)"******"

              !Call RecPrt('contr_int ANGmom From GEN1INT in Molcas Format',&
              !               ' ',contr_int,&
              !             num_contr_bra*num_contr_ket, &
              !             num_sgto_bra*num_sgto_ket*num_prop)

      call xtimer_set(end_time)
    !deallocate(contr_int)
    deallocate(contr_ints)
    return
  end subroutine test_f90mod_sgto_angmom
